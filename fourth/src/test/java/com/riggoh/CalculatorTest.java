package com.riggoh;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testBaseFunctionality() {
        Assert.assertEquals("Unexpected value", 4, Calculator.sum(2, 2));
    }
}
